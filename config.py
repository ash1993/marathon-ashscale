#config file for marathon-autoscale, DO NOT CHANGE VARIABLE NAMES

marathon_host_conf = 'localhost:8080'
marathon_app_conf = 'test'
sleep_cycle_conf = 10
trigger_mode_conf = 'or'
max_mem_percent_conf = 1
max_cpu_time_conf = 1
autoscale_multiplier_conf = 2
max_instances_conf = 5
usr_conf = 'test'
pwd_conf = 'test'