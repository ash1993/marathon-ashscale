__author__ = 'Ash'

import sys
import requests
import json
import math
import time
import os.path
import getpass


payload = {'message': '','marathon_app': '', 'marathon_host': '', 'instance_ids': '', 'maximum_instances': '', 'autoscale_reason': ''}
usr =''
pwd =''

if os.path.isfile('config.py'):
    from config import *
    marathon_host = marathon_host_conf
    marathon_app = marathon_app_conf
    sleep_cycle = sleep_cycle_conf
    max_mem_percent = max_mem_percent_conf
    max_cpu_time = max_cpu_time_conf
    autoscale_multiplier = autoscale_multiplier_conf
    max_instances = max_instances_conf
    trigger_mode = trigger_mode_conf
    usr = usr_conf
    pwd = pwd_conf



else:
    print("config file doesn't exist, please enter your configuration manually")
    while True:
        marathon_host = input("Enter the DNS hostname or IP of your Marathon Instance : ")
        if not marathon_host:
            print("Please enter a value!")
            continue
        
        else:
            try:
                requests.get("http://"+marathon_host)
            except requests.exceptions.ConnectionError as e:
                print ("Your marathon instance does not exist, please enter a different IP or hostname!")
                continue
            
            break

    test_response = requests.get("http://"+marathon_host)
    if test_response.status_code == 401:
        print('It seems like your marathon instance is protected, please provide a username and password')
        while True:
            usr = input("Username: ")
            if not usr:
                print("Please enter a value!")
                continue
            else:
                break
        while True:
            pwd.getpass.getpass('Password: ')
            if not pwd:
                print("Please enter a value!")
                continue
            else:
                break

    while True:
        marathon_app = input("Enter the Marathon Application Name to Configure Autoscale for from the Marathon UI : ")
        if not marathon_app:
            print("Please enter a value!")
            continue
        else:
            break

    while True:
        try:
            sleep_cycle = float(input("Define your sleep cycle interval, enter your sleep time in seconds: "))
        except ValueError:
            print("Please enter a integer value ONLY and try again")
            continue
        else:
            break 

    while True:

        try:
            max_mem_percent = int(input("Enter the Max percent of Mem Usage averaged across all Application Instances to trigger Autoscale (ie. 80) : "))
        except ValueError:
            print("Please enter an integer value ONLY (ie. 80) and try again")
            continue
        if max_mem_percent > 100 or max_mem_percent < 0:
            print(max_mem_percent, "is not a valid percentage, please make sure your Memory Usage percent is between 0 and 100!")
            continue
        else:
            break

    while True:
        try:
            max_cpu_time = int(input("Enter the Max percent of CPU Usage averaged across all Application Instances to trigger Autoscale (ie. 80) : "))
            
        except ValueError:
            print("Please enter an integer value ONLY (ie. 80) and try again")
            continue
        if max_cpu_time > 100:
            print(max_cpu_time, "is not a valid percentage, please make sure your CPU Usage percent is between 0 and 100!")
            continue
        else:
            break    

    trigger_mode = input("Enter which metric(s) to trigger Autoscale ('and', 'or') : ")

    while True:
        try:
            autoscale_multiplier = float(input("Enter Autoscale multiplier for triggered Autoscale (ie 1.5) : "))
        except ValueError:
            print("Please enter a integer value ONLY and try again")
            continue
        else:
            break   

    while True:
        try:
            max_instances = int(input("Enter the Max instances that should ever exist for this application (ie. 20) : "))
        except ValueError:
            print("Please enter a integer value ONLY and try again")
            continue
        else:
            break

#marathon_host = 'thomaskra-elasticl-1dw3edm5f8i3m-2072358235.us-east-1.elb.amazonaws.com'
 
payload['maximum_instances'] = max_instances

class marathon(object):

    def __init__(self,marathon_host):
        self.name=marathon_host
        self.uri=("http://"+marathon_host)

    

    def get_all_apps(self):
        response = requests.get(self.uri + '/v2/apps', auth=(usr,pwd)).json()
        

        # if (response.status_code == 401):
        #     print("It appears that your marathon instance is protected, please enter your username and password")
        #     usr = input("Username: ")
        #     pwd = getpass.getpass("Password: ")
        #     response = requests.get(self.uri + '/v2/apps', auth=(usr,pwd)).json()
            
        # else:
        #     response = requests.get(self.uri + '/v2/apps').json()

        if response['apps'] ==[]:
            print ("No Apps found on Marathon")
            sys.exit(1)
        else:
            apps=[]
            for i in response['apps']:
                appid=i['id'].strip('/')
                apps.append(appid)
            print ("Found the following App LIST on Marathon =",apps)
            self.apps=apps
            return apps

    def get_app_details(self,marathon_app):
        response=requests.get(self.uri + '/v2/apps/'+ marathon_app, auth=(usr,pwd)).json()
        if (response['app']['tasks'] ==[]):
            print ('No task data on Marathon for App !', marathon_app)
        else:
            app_instances=response['app']['instances']
            self.appinstances=app_instances
            print(marathon_app,"has",self.appinstances,"deployed instances")
            payload['marathon_app'] = marathon_app
            payload['marathon_host'] = self.name
            app_task_dict={}
            for i in response['app']['tasks']:
                taskid=i['id']
                hostid=i['host']
                print ('DEBUG - taskId=',taskid +' running on '+hostid)
                app_task_dict[str(taskid)]=str(hostid)
            return app_task_dict

    def scale_app(self,marathon_app,autoscale_multiplier):
        target_instances_float=self.appinstances * autoscale_multiplier
        target_instances=math.ceil(target_instances_float)
        if (target_instances > max_instances):
            print("Reached the set maximum instances of", max_instances)
            target_instances=max_instances
        else:
            target_instances=target_instances
        data ={'instances': target_instances}
        json_data=json.dumps(data)
        headers = {'Content-type': 'application/json'}
        response=requests.put(self.uri + '/v2/apps/'+ marathon_app,json_data,headers=headers,auth=(usr,pwd))
        print ('Scale_app return status code =', response.status_code)
        if (response.status_code == 200) and (target_instances < max_instances):
            payload['message'] = 'Successfully autoscaled app: ' + marathon_app
        else:
            payload['message'] = 'The app has failed to autoscale anymore, this is probably due to reaching your maximum instance threshold'


def get_task_agentstatistics(task,host):
    # Get the performance Metrics for all the tasks for the Marathon App specified
    # by connecting to the Mesos Agent and then making a REST call against Mesos statistics
    # Return to Statistics for the specific task for the marathon_app
    response=requests.get('http://'+host + ':5051/monitor/statistics.json', auth=(usr,pwd)).json()
    #print ('DEBUG -- Getting Mesos Metrics for Mesos Agent =',host)
    for i in response:
        executor_id=i['executor_id']
        #print("DEBUG -- Printing each Executor ID ",executor_id)
        if (executor_id == task):
            task_stats =i['statistics']
            # print ('****Specific stats for task',executor_id,'=',task_stats)
            return task_stats
def timer():
    print("Successfully completed a cycle, sleeping for",sleep_cycle,"seconds ...")
    # Snippet example on how to POST the payload to a REST endpoint
    # url = 'http://localhost:3000/apps'
    # headers = {'Content-type': 'application/json'}
    # data = json.dumps(payload)
    # print(data)
    # r = requests.post(url, data=json.dumps(payload),headers=headers)
    # print(r.status_code)
    print(payload)
    time.sleep(sleep_cycle)
    return

if __name__ == "__main__":
    print ("This application tested with Python3 only")
    running=1
    while running == 1:
        # Initialize the Marathon object
        aws_marathon=marathon(marathon_host)
        # Call get_all_apps method for new object created from aws_marathon class and return all apps
        marathon_apps = aws_marathon.get_all_apps()
        print ("The following apps exist in Marathon...", marathon_apps)
        # Quick sanity check to test for apps existence in MArathon.
        if (marathon_app in marathon_apps):
            print ("  Found your Marathon App=",marathon_app)
        else:
            print ("  Could not find your App =",marathon_app)
            sys.exit(1)
        # Return a dictionary comprised of the target app taskId and hostId.
        app_task_dict = aws_marathon.get_app_details(marathon_app)
        print ("    Marathon  App 'tasks' for", marathon_app, "are=", app_task_dict)
        
        appList = [{k:v} for k, v in app_task_dict.items()]

        payload['instance_ids'] = appList

        app_cpu_values=[]
        app_mem_values=[]

        for task,host in app_task_dict.items():
            task_stats=get_task_agentstatistics(task,host)
            cpus_time =(task_stats['cpus_system_time_secs']+task_stats['cpus_user_time_secs'])
            print ("Combined Task CPU Kernel and User Time for task", task,"=",cpus_time)
            mem_rss_bytes = int(task_stats['mem_rss_bytes'])
            print ("task",task, "mem_rss_bytes=",mem_rss_bytes)
            mem_limit_bytes = int(task_stats['mem_limit_bytes'])
            print ("task",task,"mem_limit_bytes=",mem_limit_bytes)
            mem_utilization=100*(float(mem_rss_bytes) / float(mem_limit_bytes))
            print ("task", task, "mem Utilization=",mem_utilization)
            print()
            app_cpu_values.append(cpus_time)
            app_mem_values.append(mem_utilization)
        # Normalized data for all tasks into a single value by averaging
        app_avg_cpu = (sum(app_cpu_values) / len(app_cpu_values))
        print ('Current Average  CPU Time for app', marathon_app,'=', app_avg_cpu)
        app_avg_mem=(sum(app_mem_values) / len(app_mem_values))
        print ('Current Average Mem Utilization for app', marathon_app,'=', app_avg_mem)
        #Evaluate whether an autoscale trigger is called for
        print('\n')
        if (trigger_mode=="and"):
            if (app_avg_cpu > max_cpu_time) and (app_avg_mem > max_mem_percent):
                print ("Autoscale triggered based on 'both' Mem & CPU exceeding threshold")
                payload['autoscale_reason'] = "Autoscale triggered based on 'both' Mem & CPU exceeding threshold"
                aws_marathon.scale_app(marathon_app,autoscale_multiplier)
                timer()
            else:
                print ("Both values were not greater than autoscale targets")
                timer()
        elif (trigger_mode=="or"):
            if (app_avg_cpu > max_cpu_time) or (app_avg_mem > max_mem_percent):
                print ("Autoscale triggered based Mem 'or' CPU exceeding threshold")
                payload['autoscale_reason'] = "Autoscale triggered based Mem 'or' CPU exceeding threshold"
                aws_marathon.scale_app(marathon_app,autoscale_multiplier)
                timer()
            else:
                print ("Neither Mem 'or' CPU values exceeding threshold")
                timer()

